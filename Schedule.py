import networkx as nx;
import copy;
from decimal import *;
import matplotlib.pyplot as plt;
from matplotlib.patches import Rectangle;
from matplotlib.lines import Line2D;
import TaskSet as ts;
import SchedulabilityTest;
from sys import argv;
import pickle;
import time;


class Schedule:
    """
    Represent a constructed schedule formed of several templates.
    """
    
    def __init__(self, schedulabilityTest, taskSet):
        self.__templates = [];
        partitions = schedulabilityTest.getPartitions()
        assignments = schedulabilityTest.getAssignments()
        for i in range(len(partitions)) :
            self.__templates.append(Template(taskSet.getConfiguration().nbProcessors, taskSet, assignments[i], partitions[i]));
            
    def getFailureRatio(self):
        count = 0;
        for i in self.__templates:
            if(i.error):
                count += 1;
        return count/len(self.__templates)
            
    def showTemplate(self, index):
        if(index < len(self.__templates) and index >= 0):
            print(self.__templates[index].getTemplate());
        else:
            print("Template out of bounds.");
            
    def plotTemplate(self, index):
        if(index < len(self.__templates) and index >= 0):
            self.__templates[index].plot(index);
        else:
            print("Template out of bounds.");
            
    def reportTemplates(self):
        for i in range(len(self.__templates)):
            self.showTemplate(i);
            self.plotTemplate(i);

class Template:
    """
    Represent a schedule temaple.
    """
    
    def __init__(self, nbProcessors, taskSet, assignment, partition):
        self.error = False;
        self.__nbProcessors = nbProcessors;
        self.__taskSet = taskSet;
        self.__partition = partition;
        self.__originalAssignment = assignment;
        self.__assignment = assignment.getDict();
        self.__t = Decimal(1);
        self.__template = [[] for i in range(nbProcessors)]; # each sub-list is a processor, contains tuples (phase nb, time start excluded, time of stop included) ==> pay attention, start and stop in reverse
        self.__chosenPhases  = [];
        self.__chosenProcessors = [];
        self.__matching = [];
        self.__construct();
    
    
    def plot(self,index):
        """
        Plot the template (and display the index in the title of the figure).
        """
        fig, ax = plt.subplots();
        cmap = plt.cm.gnuplot2;
        for processor in range(0,len(self.__template)):
            for event in self.__template[processor]:
                processor_display = processor + 1;
                rectangle = Rectangle((event[2], processor_display-1), event[1]-event[2], 1, color=cmap(30+((event[0][0]-1)*30)+((event[0][1]-1)*2)));
                ax.add_patch(rectangle);
        bounds = set();
        for processor in self.__template:
            for event in processor:
                if(event[1] != 1 and event[1] != 0):
                    bounds.add(event[1]);
                if(event[2] != 1 and event[2] != 0):
                    bounds.add(event[2]);
        for bound in bounds:
            line = Line2D([bound,bound],[0,len(self.__template)+0.5],linestyle="--",color="black");
            ax.add_artist(line);
        legendLines = [Line2D([0], [0], color=cmap(30+((self.__partition[0][i]-1)*30)+((self.__partition[1][i]-1)*2)), lw=4) for i in range(len(self.__partition[0]))];
        legendText = ["Phase "+str(self.__partition[0][i])+"_"+str(self.__partition[1][i]) for i in range(len(self.__partition[0]))];
        ax.legend(legendLines, legendText, bbox_to_anchor=(1,1), loc="upper left");
        ax.set_yticks([i for i in range(len(self.__template)+1)], minor=True);
        ax.set_yticks([i+0.5 for i in range(len(self.__template))]);
        ax.set_yticklabels([str(i) for i in range(1,len(self.__template)+1)]);
        ax.grid(which='minor', axis="y");
        ax.grid(True, axis="x");
        ax.set_xlabel("Time");
        ax.set_ylabel("Processor");
        plt.title("Template "+str(index+1));
        plt.show();
    
    
    def __construct(self):
        """
        Iteratively construct the template.
        """
        check = True;
        deltas = []
        self.deltacandidates = []
        while(check and self.__notEnd()):
            matching = self.__generateMatching();
            check = self.__checkMatching(matching);
            delta = self.__chooseDelta(matching);
            deltas.append(delta)
            self.__stepTemplate(matching, delta);
            if(self.__problem() or not check):
                check = False;
        if(not check):
            print("Error")
            self.error = True;
    
    
    def __notEnd(self):
        """
        Tells if the construction routine needs to continue iterating (whether or not all the fractions of time have been assigned in the template).
        """
        for key in self.__assignment:
            if(sum(self.__assignment[key]) > 0):
                return True;
        return False;
            
    
    def __problem(self):
        if(self.__t < 0):
            print("problem 1")
            return True;
        return False;
    
    
    def __generateMatching(self):
        """
        Create the matching in the bipartit graph representing the processors and phases.
        """
        graphFull = self.__constructFullGraph();
        graphUrgent = self.__constructUrgentGraph();
        matchFull = list(nx.max_weight_matching(graphFull));
        matchUrgent = list(nx.max_weight_matching(graphUrgent));
        
        graphFusion = nx.Graph();
        for k in range(0,self.__taskSet.getConfiguration().nbProcessors):
            graphFusion.add_node(k);
        for i in range(len(self.__partition[0])):
            graphFusion.add_node((self.__partition[0][i],self.__partition[1][i]));
        for edge in matchFull:
            if(edge not in graphFusion.edges):
                graphFusion.add_edge(edge[0],edge[1]);
        for edge in matchUrgent:
            if(edge not in graphFusion.edges):
                graphFusion.add_edge(edge[0],edge[1]);
        graphFusion.remove_nodes_from([node for node,degree in dict(graphFusion.degree()).items() if degree == 0]);
        
        self.__removeExcessEdges(graphFusion);
        graphFusion.remove_nodes_from([node for node,degree in dict(graphFusion.degree()).items() if degree == 0]);
        
        matchFusion = list(graphFusion.edges);
        
        matching = matchFusion;
        return dict(list({edge[0]:edge[1] for edge in matching}.items()) + list({edge[1]:edge[0] for edge in matching}.items()))
    
    
    def __removeExcessEdges(self, graph):
        """
        Remove the edges in excess to obtain a true matching.
        """
        importants = self.__GetAllFull()+self.__getAllUrgent();
        components = nx.connected_components(graph);
        degrees = dict(graph.degree());
        for component in components:
            component = list(component);
            if(len(component) > 2):
                importantOneDegree = None;
                for i in component:
                    if(i in importants and degrees[i] == 1):
                        importantOneDegree = i;
                        break;
                start = None;
                if(importantOneDegree == None):
                    start = component[0];
                else:
                    start = importantOneDegree;
                traversal = [];
                initial = start;
                traversal.append((1,(start, list(graph.neighbors(start))[0])));
                i=2;
                preceding = start;
                start = list(graph.neighbors(start))[0];
                while((not start == initial) and (not len(list(graph.neighbors(start))) == 1)):
                    for neigbor in graph.neighbors(start):
                        if(not neigbor == preceding):
                            preceding = start;
                            traversal.append((i,(start,neigbor)));
                            start = neigbor;
                            break;
                    i += 1;
                for i in range(1,len(traversal),2):
                    graph.remove_edge(traversal[i][1][0],traversal[i][1][1]);
    
    
    def __constructFullGraph(self):
        """
        Construct a graph containing only the full processors (and associated phases).
        """
        fulls = self.__GetAllFull();
        graph = nx.Graph();
        if(not len(fulls) == 0):
            for i in fulls:
                graph.add_node(i);
            for i in range(len(self.__partition[0])):
                graph.add_node((self.__partition[0][i],self.__partition[1][i]));
            for i in range(len(self.__partition[0])):
                for k in fulls:
                    if(self.__assignment[str(self.__partition[0][i])+"_"+str(self.__partition[1][i])][k] > 0):
                        graph.add_edge( (self.__partition[0][i],self.__partition[1][i]), k);
            graph.remove_nodes_from([node for node,degree in dict(graph.degree()).items() if degree == 0]);
        return graph;
        
        
    def __constructUrgentGraph(self):
        """
        Construct a graph containing only the urgent phases and associated processors.
        """
        urgents = self.__getAllUrgent();
        graph = nx.Graph();
        if(not len(urgents) == 0):
            for i in urgents:
                graph.add_node(i);
            for i in range(0,self.__taskSet.getConfiguration().nbProcessors):
                graph.add_node(i);
            for i in urgents:
                for k in range(0,self.__taskSet.getConfiguration().nbProcessors):
                    if(self.__assignment[str(i[0])+"_"+str(i[1])][k] > 0):
                        graph.add_edge( i, k);
            graph.remove_nodes_from([node for node,degree in dict(graph.degree()).items() if degree == 0]);
        return graph;
    
    
    def __constructResidueGraph(self, inFusion):
        """
        Construct a graph containing all the non already matched processors and phases (the parameter inFusion needs to be a list of vertices present in the graph containing the already existing matching).
        """
        graph = nx.Graph();
        processorNonFusion = [];
        phaseNonFusion = [];
        for k in range(0,self.__taskSet.getConfiguration().nbProcessors):
            if(k not in inFusion):
                graph.add_node(k);
                processorNonFusion.append(k);
        for i in range(len(self.__partition[0])):
            if((self.__partition[0][i],self.__partition[1][i]) not in inFusion):
                graph.add_node((self.__partition[0][i],self.__partition[1][i]));
                phaseNonFusion.append((self.__partition[0][i],self.__partition[1][i]));
        for i in phaseNonFusion:
            for k in processorNonFusion:
                if(self.__assignment[str(i[0])+"_"+str(i[1])][k] > 0):
                    graph.add_edge( i, k);
        graph.remove_nodes_from([node for node,degree in dict(graph.degree()).items() if degree == 0]);
        return graph;

    
    def __checkMatching(self, match):
        """
        Check if the matching contains all the urgent phases and full processors.
        """
        urgents = self.__getAllUrgent();
        fulls = self.__GetAllFull();
        for i in urgents:
            if(i not in match):
                print("problem 2")
                return False;
        for i in fulls:
            if(i not in match):
                print("problem 2")
                return False;
        return True;
            
    
    def __chooseDelta(self,match):
        """
        Choose the delta value for the current step.
        """
        candidates = set();
        for i in match:
            if(type(i) == tuple):
                candidates.add(self.__assignment[str(i[0])+"_"+str(i[1])][match[i]]);
        for i in range(len(self.__partition[0])):
            if((self.__partition[0][i],self.__partition[1][i]) not in match):
                candidates.add(self.__t - sum(self.__assignment[str(self.__partition[0][i])+"_"+str(self.__partition[1][i])]));
        for k in range(0,self.__taskSet.getConfiguration().nbProcessors):
            if(k not in match):
                sumk = Decimal('0');
                for phase in self.__assignment:
                    sumk += self.__assignment[phase][k];
                candidates.add(self.__t - sumk);
        self.deltacandidates.append(candidates)
        return min(candidates);
                
    
    def __stepTemplate(self,match,delta):
        """
        Perform the step in the template generation using the created matching and the chosen delta value.
        """
        for m in match:
            if(type(m) == tuple):
                self.__template[match[m]].append((m,self.__t,Decimal(self.__t - delta)));
                self.__assignment[str(m[0])+"_"+str(m[1])][match[m]] -= delta;
        self.__t -= delta;
    
    
    def __getAllUrgent(self):
        """
        Returns all the urgent phases.
        """
        list = [];
        for i in range(len(self.__partition[0])):
            if(self.__isUrgent(str(self.__partition[0][i])+"_"+str(self.__partition[1][i]))):
                list.append((self.__partition[0][i],self.__partition[1][i]));
        return list;
                
    
    def __GetAllFull(self):
        """
        Returns all the full processors.
        """
        list = [];
        for k in range(0,self.__taskSet.getConfiguration().nbProcessors):
            if(self.__isFull(k)):
                list.append(k);
        return list;
    
    
    def __isUrgent(self, phaseName):
        if(sum(self.__assignment[phaseName]) >= self.__t):
            return True;
        return False;
    
    
    def __isFull(self, processorNb):
        sumk = Decimal(0);
        for phase in self.__assignment:
            sumk += self.__assignment[phase][processorNb];
        if(sumk >= self.__t):
            return True;
        return False;
    
    def getTemplate(self):
        return copy.deepcopy(self.__template);


if __name__ == "__main__":
    """
    Usage: python3 Schedule.py: launch the test experiment
           python3 Schedule.py b: launch the complete experiment with time collection
    """
    getcontext().prec = 100
    
    if(len(argv) == 1): # test experiment
    
        configuration = ts.Configuration();
        configuration.lowerNbTasksBound = 5;
        configuration.upperNbTasksBound = 5;
        configuration.minNumberPhases = 6;
        configuration.maxNumberPhases = 6;
        
        taskset = ts.TaskSetSimpleGeneral(configuration);
        #taskset.inisializeRandom();
        taskset.inisializeFromList([[0,15,15,[[0,4,12,[1,3,45,2,3,2],[1,1,0,0,1,1]],[12,5,15,[3,5,58,4,9,7],[1,1,0,1,1,1]]]],[0,15,15,[[0,10,10,[2,2,2,2,2,2],[1,1,1,1,1,1]],[10,2,13,[1,1,2,2,1,1],[0,0,1,1,0,0]]]],[0,15,15,[[0,7,9,[1,1,3,2,1,2],[1,0,1,1,1,1]],[9,6,15,[2,2,2,2,2,2],[1,1,1,1,1,1]]]],[0,15,15,[[0,6,5,[2,3,5,6,7,8],[1,1,0,1,1,1]],[5,4,15,[2,2,1,1,1,1],[1,1,0,0,0,0]]]],[0,15,15,[[0,20,6,[5,4,6,7,5,5],[1,1,1,1,1,1]],[6,10,15,[4,4,4,0,0,0],[1,1,1,0,0,0]]]]]);
        print(taskset)
        test = SchedulabilityTest.SchedulabilityTest(taskset);
        test.run();
        
        if(test.isSuccessfull()):
            schedule = Schedule(test, taskset);
            schedule.reportTemplates();
        else:
            print("TaskSet not schedulable.")
            
    else:
        
        data = [];
        dataFail = [];
        dataCount = [];
        totalCount = 0;
        for size in range(15,51,5):
            configuration = ts.Configuration();
            configuration.lowerNbTasksBound = 5;
            configuration.upperNbTasksBound = 5;
            configuration.minNumberPhases = size;
            configuration.maxNumberPhases = size;
            configuration.maxProcessorSpeed = 7;
            configuration.maximalCPhase = 12;
            timeAvg = 0;
            count = 0;
            failures = 0;
            for i in range(1000):#1000
                taskset = ts.TaskSetSimpleGeneral(configuration);
                taskset.inisializeRandom();
                test = SchedulabilityTest.SchedulabilityTest(taskset);
                test.run();
                if(test.isSuccessfull()):
                    for j in range(4): #4
                        startTime = time.perf_counter_ns();
                        schedule = Schedule(test, taskset);
                        endTime = time.perf_counter_ns();
                        timeTaken = endTime - startTime;
                        timeAvg += timeTaken;
                        count += 1;
                        failures += schedule.getFailureRatio();
            if(count == 0):
               data.append(0);
               dataFail.append(0);
               dataCount.append(0);
            else:
                timeAvg /= count;
                failures /= count;
                totalCount += count;
                data.append(timeAvg);
                dataFail.append(failures);
                dataCount.append(count/4);
        print(totalCount)
        
        with open("dataTimeCreationTemplates.save", 'wb') as file0:
            pickle.dump(data, file0);
            
        with open("dataFailureCreationTemplates.save", 'wb') as file0:
            pickle.dump(dataFail, file0);
            
        with open("dataCountCreationTemplates.save", 'wb') as file0:
            pickle.dump(dataCount, file0);
            
        fig, ax = plt.subplots();
        ax.plot([i for i in range(15,51,5)],data);
        ax.set(xlabel="number of phases for each task", ylabel="average time taken (nanoseconds)");
        ax.grid();
        plt.title("Templates Creation");
        plt.show();
        
        fig, ax = plt.subplots();
        ax.bar([i for i in range(15,51,5)],dataFail);
        ax.set(xlabel="number of phases for each task", ylabel="average failure ratio");
        ax.grid();
        plt.title("Templates Creation Fails");
        plt.show();
        
        fig, ax = plt.subplots();
        ax.bar([i for i in range(15,51,5)],dataCount);
        ax.set(xlabel="number of phases for each task", ylabel="number of schedulable task sets");
        ax.grid();
        plt.title("Templates Creation Schedulable");
        plt.show();
    
