# Real-time scheduling of multiphase tasks on unrelated parallel platforms

This implementation targets the schedulability of real-time tasks sets composed of sequential multiphase tasks featuring the same deadline for every task.

Therefore, this implementation comprises a schedulability test (featuring a polynomial time execution and the exact property for global scheduling) as well as routines for template schedule creation.

## Requirements

The implementation is done in python3. And it depends on the packages:
* matplotlib
* pickle
* pulp
* networkx
* decimal

In addition, the [GLPK](https://www.gnu.org/software/glpk/) solver needs to be properly installed and present in the PATH.

## Usage

The implementation proposes two benchmarks, the first one can be launched with the file Experiment.py and the second with the file Schedule.py.

Indeed, the file Experiment.py will measure the time taken by the schedulability test to execute on random task sets and can be called using the following formatting:

	python3 Experiment.py ttype nbTasks minP maxP step avgOver prec pref

Where ttype is either general or harmonic or strict and is the type of the task set to be used, nbTasks is the number of tasks that each task set will contain, minP and MaxP are the minimal and the maximal number of phases that each task will contain, step is the increment used to go from MaxP to MinP (in order to create a plot in function of the number of phases), avgOver is the number of task sets generated for a same number of phases, prec is the number of time each generated task set is tested in the schedulability test (in order to smoth variations in the measures) and pref is the prefix of the files names saving the data using pickle.

And the file Schedule.py can be called with no parameters in which case it will generate the templates for a hardcoded task set example. Or with a parameter (which value does not matter), in which case it will run the experiment (measure of the time taken by the template creation algorithm on random task sets) and output some plots featuring the results.