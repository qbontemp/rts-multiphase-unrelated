import pulp;
import TaskSet as ts;
from decimal import *;

class Assignment:
    """
    Interface between the program and the solver (implements shcedulability test that minimizes the make-span).
    Use the pulp library (https://coin-or.github.io/pulp/CaseStudies/a_blending_problem.html) with GLPK as back-end.
    sUsage: after construction, call the solve method and collect the results with the getters.
    """
    
    def __init__(self, partition, taskSet):
        """
        parameters: - partition: the group of phases
                    - taskSet: the task set itself
        """
        self.partition = partition;
        self.taskSet = taskSet;
        self.prob = pulp.LpProblem("Assignment", pulp.LpMinimize);
        self.L = pulp.LpVariable("L", 0, None, pulp.LpContinuous);
        self.X = pulp.LpVariable.dicts("time_assignment_",[str(partition[0][i])+"_"+str(partition[1][i])+"_"+str(k) for i in range(len(partition[0])) for k in range(1,taskSet.getConfiguration().nbProcessors+1)],0,None,pulp.LpContinuous);
        self.prob += self.L , "Minimize the makespan";
        # two first sets of constraints
        for phase in range(len(partition[0])):
            i = partition[0][phase];
            j = partition[1][phase];
            self.prob += pulp.lpSum([self.X[str(i)+"_"+str(j)+"_"+str(k)] * taskSet.getTask(i).getPhase(j).getSpeed(k) for k in range(1,taskSet.getConfiguration().nbProcessors+1)]) == taskSet.getTask(i).getPhase(j).getUtilisation(),"Phase " + str(i) +" "+ str(j) + " meet their utilisation";
            self.prob += pulp.lpSum([self.X[str(i)+"_"+str(j)+"_"+str(k)] for k in range(1,taskSet.getConfiguration().nbProcessors+1)])  <= self.L ,"Phase " + str(i) +" "+ str(j) + " does not exceed the makespan";
        # last set of constraints
        for k in range(1,taskSet.getConfiguration().nbProcessors+1):
            self.prob += pulp.lpSum([self.X[str(partition[0][i])+"_"+str(partition[1][i])+"_"+str(k)] for i in range(len(partition[0]))]) <= self.L,"Processor "+str(k)+" does not exceed the makespan";
    
    def solve(self, maxTime=1200): # time in seconds 1200s = 20min (maxTime == maximal time allowed for the solver to run)
        self.prob.solve(pulp.GLPK(options=["--tmlim", str(maxTime)]));
        
    def getTimeTaken(self):
        return self.prob.solutionTime;
    
    def getMakeSpan(self):
        return Decimal(self.L.value());
    
    def isFeasible(self):
        feasible = True;
        if(self.getMakeSpan() > 1):
            feasible = False;
        else: # some times even with a good makespan, the solver returns a solution with a phase executing for more than possible (or a processor too much loaded), likely due to GLPK roundings
            dico = self.getDict();
            for phase in dico:
                if(sum(dico[phase]) > Decimal("1")):
                    feasible = False;
                    break;
            if(feasible):
                for i in range(self.taskSet.getConfiguration().nbProcessors):
                    sumk = Decimal("0")
                    for phase in dico:
                        sumk += dico[phase][i]
                    if(sumk > Decimal("1")):
                        feasible = False;
                        break;
        return feasible;
    
    def getDict(self):
        dict = {};
        for i in range(len(self.partition[0])):
            list = [];
            for k in range(1,self.taskSet.getConfiguration().nbProcessors+1):
                list.append(Decimal(str((self.X[str(self.partition[0][i])+"_"+str(self.partition[1][i])+"_"+str(k)]).value())));
            dict[str(self.partition[0][i])+"_"+str(self.partition[1][i])] = list;
        return dict;
    
    """def getDictFloat(self):
        dict = {};
        for i in range(len(self.partition[0])):
            list = [];
            for k in range(1,self.taskSet.getConfiguration().nbProcessors+1):
                #list.append(Decimal((self.X[str(self.partition[0][i])+"_"+str(self.partition[1][i])+"_"+str(k)]).value()));
                list.append((self.X[str(self.partition[0][i])+"_"+str(self.partition[1][i])+"_"+str(k)]).value());
            dict[str(self.partition[0][i])+"_"+str(self.partition[1][i])] = list;
        return dict;"""
    


