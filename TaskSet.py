import random;
import copy;

class Configuration:
    """
    Simple class meant to contains the necessary configurations values to generate random task sets (interval and fixed values to increase flexibility whithout touching method signatures).
    """
    
    def __init__(self):
        self.lowerNbTasksBound = 1
        self.upperNbTasksBound = 10
        self.minPeriodTaskOffset = 5
        self.maxPeriodTaskOffset = 10
        self.minimalCPhase = 1
        self.maximalCPhase = 15
        self.minOffsetDeadlinePhase = 3
        self.maxOffsetDeadlinePhase = 16
        self.nbProcessors = 6
        self.minProcessorSpeed = 1
        self.maxProcessorSpeed = 10
        self.maxAffinityDeny = 2
        self.minNumberPhases = 1
        self.maxNumberPhases = 10

class TaskSetSimpleGeneral:
    """
    Base class for a task set with no other restriciton than having deadlines at phase level.
    """
    
    def __init__(self, configuration):
        self._tasks = [];
        self._configuration = configuration;
        
    def inisializeRandom(self):
        nbTasks = random.randint(self._configuration.lowerNbTasksBound, self._configuration.upperNbTasksBound);
        for i in range(nbTasks):
            self._tasks.append(SimpleTask(self._configuration));
        # ensure all task deadlines are the same
        self._normalizeDeadlines();
    
    def _normalizeDeadlines(self):
        # ensure all task deadlines are the same
        deadlines = 0;
        for i in self._tasks:
            if(i.getDeadline() > deadlines):
                deadlines = i.getDeadline();
        for i in self._tasks:
            i.setDeadline(deadlines);
    
    def inisialize(self, tasks):
        self._tasks = tasks;
        
    def inisializeFromList(self, initList):
        for task in initList:
            self._tasks.append(SimpleTask(self._configuration));
            self._tasks[-1].inisializeFromList(task, self._configuration);
        
    def getConfiguration(self):
        return self._configuration;
        
    def getNumberTasks(self):
        return len(self._tasks);
    
    def getTask(self, index):
        return self._tasks[index-1];
    
    def __str__(self):
        stringP = "";
        for i in range(len(self._tasks)-1):
            stringP += "-{} := {}\n".format(i+1,self._tasks[i].__str__());
        stringP += "-{} := {}\n".format(len(self._tasks),self._tasks[-1].__str__());
        return "The task set contains {} tasks distributed on {} processors.\nTasks:\n{}".format(len(self._tasks),self._configuration.nbProcessors,stringP);

class TaskSetSimpleSA(TaskSetSimpleGeneral):
    """
    Class representing a task set with a same freqency relashion between the tasks.
    """
    
    def __init__(self, configuration):
        super().__init__(configuration);
    
    def inisializeRandom(self):
        nbTasks = random.randint(self._configuration.lowerNbTasksBound, self._configuration.upperNbTasksBound);
        self._tasks.append(SimpleTask(self._configuration));
        for i in range(nbTasks-1):
            self._tasks.append(copy.deepcopy(self._tasks[0]));
            self._tasks[-1].shuffleCR(self._configuration);

class TaskSetSimpleHarmonic(TaskSetSimpleGeneral):
    """
    Class representing a task set with an harmonic relashion between phases.
    """
    
    def __init__(self, configuration, baseFrequency=2):
        super().__init__(configuration);
        self._baseFrequency = baseFrequency;
    
    def inisializeRandom(self):
        nbTasks = random.randint(self._configuration.lowerNbTasksBound, self._configuration.upperNbTasksBound);
        baseSize = 2**(nbTasks-1);
        self._tasks.append(SimpleTask(self._configuration,1,baseSize));
        for i in range(self._baseFrequency,self._baseFrequency*nbTasks,self._baseFrequency):
            baseSize /= 2;
            self._tasks.append(SimpleTask(self._configuration,i,baseSize));
        self._normalizeDeadlines();

class Task:
    """
    Base class for a task.
    """
    
    def __init__(self):
        self._phases = [];
        self._precedence = [];
        self._offset = 0;
        self._period = 0;
        self._deadline = 0;
        
    def getNumberPhases(self):
        return len(self._phases);
    
    def getPhase(self, index):
        return self._phases[index-1];
    
    def getOffset(self):
        return self._offset;
    
    def getDeadline(self):
        return self._deadline;
    
    def setDeadline(self, deadline):
        self._deadline = deadline;
        self._period = deadline;
        
    def __str__(self):
        phasesString = "";
        for i in range(len(self._phases)-1):
            phasesString += "    {} := {}\n".format(i+1,self._phases[i].__str__());
        phasesString += "    {} := {}".format(len(self._phases),self._phases[-1].__str__());
        return "offset: {}, deadline: {}, period: {}\nphases:\n{}".format(self._offset,self._deadline,self._period,phasesString);


class SimpleTask(Task):
    """
    Represent a sequencial task.
    """
    
    def __init__(self, configuration, frequency=-1, baseSize=-1):
        super().__init__();
        if(frequency == -1):
            nbPhases = random.randint(configuration.minNumberPhases, configuration.maxNumberPhases);
        else:
            nbPhases = frequency;
        for i in range(nbPhases):
            if(len(self._phases) != 0):
                self._phases.append(PhaseConstrained(self._phases[-1].getDeadline(), configuration, baseSize));
            else:
                self._phases.append(PhaseConstrained(0, configuration, baseSize));
        self._period = random.randint(self._phases[-1].getDeadline(),self._phases[-1].getDeadline()+configuration.maxPeriodTaskOffset);
        self._deadline = self._period;
        
    def inisializeFromList(self, initList, configuration):
        self._offset = initList[0];
        self._period = initList[1];
        self._deadline = initList[2];
        self._phases = [];
        for phase in initList[3]:
            self._phases.append(PhaseConstrained(0, configuration, -1));
            self._phases[-1].inisializeFromList(phase);
        
    def shuffleCR(self, configuration):
        for i in self._phases:
            i.shuffleCR(configuration);

class PhaseConstrained:
    """
    Represent a phase with deadline.
    Note that even if the offset attribute do exist, it is set to the deadline of the preceding phase and therefore is only used as a facilitation.
    """
    
    def __init__(self, precedingDeadline, configuration, baseSize=-1):
        self.shuffleCR(configuration);
        self._O = precedingDeadline;
        if(baseSize == -1):
            self._D = random.randint(self._O+configuration.minOffsetDeadlinePhase, self._O+configuration.maxOffsetDeadlinePhase);
        else:
            self._D = self._O+baseSize;
                
    def shuffleCR(self, configuration):
        self._C = random.randint(configuration.minimalCPhase, configuration.maximalCPhase);
        self._R = [];
        self._alpha = [];
        affdeny = 0;
        for i in range(configuration.nbProcessors):
            self._R.append(random.randint(configuration.minProcessorSpeed, configuration.maxProcessorSpeed));
            if(affdeny < configuration.maxAffinityDeny):
                self._alpha.append(random.randint(0,1));
                if(self._alpha[-1] == 0):
                    affdeny += 1;
            else:
                self._alpha.append(1);
                
    def inisializeFromList(self, initList):
        self._O = initList[0];
        self._C = initList[1];
        self._D = initList[2];
        self._R = initList[3];
        self._alpha = initList[4];
                
    def getSpeed(self, processorIndex):
        if(self._alpha[processorIndex-1] == 1):
            return self._R[processorIndex-1];
        else:
            return 0;
        
    def getUtilisation(self):
        return (self._C/(self._D-self._O));
    
    def getDeadline(self):
        return self._D;
    
    def getOffset(self):
        return self._O;
    
    def __str__(self):
        rString = "[";
        alphaString = "[";
        for i in range(len(self._R)-1):
            rString += "{},".format(self._R[i]);
            alphaString += "{},".format(self._alpha[i]);
        rString += "{}]".format(self._R[-1]);
        alphaString += "{}]".format(self._alpha[-1]);
        return "O: {}, D: {}, C: {}, R: {}, alpha: {}.".format(self._O,self._D,self._C,rString,alphaString);



