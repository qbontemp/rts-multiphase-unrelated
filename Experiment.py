import TaskSet as ts;
import Assignment;
import SchedulabilityTest;
import time;
import matplotlib.pyplot as plt;
import pickle;
from sys import argv;

class Experiment:
    """
    Collect data (time taken, number of groups of phases, success rates) over the schedulability test of a given task set.
    Usage: instanciate, call the run method with the amound of wanted repetitions (for averaging of the results) and use the getters to get the collected informations.
    """
    
    def __init__(self, taskSet):
        self.taskSet = taskSet;
        
    def run(self, repetitions = 10):
        """
        parameters: -repetitions: number of repetitions to compute the average
        """
        self.instancesTimes = [];
        self.timeAvg = 0;
        for i in range(repetitions):
            startTime = time.perf_counter_ns();
            self.test = SchedulabilityTest.SchedulabilityTest(self.taskSet);
            self.test.run();
            endTime = time.perf_counter_ns();
            timeTaken = endTime - startTime;
            self.timeAvg += timeTaken;
            self.instancesTimes.append(timeTaken);
        self.timeAvg /= repetitions;
      
    def getTimeSeconds(self):
        return self.timeAvg/1000000000;
    
    def getTime(self):
        return self.timeAvg;
    
    def getNbClusters(self):
        return self.test.getNbClusters();
    
    def getPrecisionErrorRation(self):
        return self.test.precisionFailureRatio();
        
    def isSuccessfull(self):
        return self.test.isSuccessfull();

class plotter:
    """
    Given a configuration, collect data related to the schedulability test of a given type of task set and provide methods to save and plot the results.
    In details, randomly generate task sets according to the configuration with more and more phases and parameters and three vectors of data are collected, 
     - the average time taken for each given number of phases in the task set
     - the average number of groups of phases for each given number of phases in the task set
     - the success rate for each given number of phases in the task set
    """
    
    def __init__(self, configuration):
        self.__typesF = {"general":ts.TaskSetSimpleGeneral,"strict":ts.TaskSetSimpleSA,"harmonic":ts.TaskSetSimpleHarmonic};
        self.__configuration = configuration;
        self.__times = [];
        self.__nbClusters = [];
        self.__nbSuccess = [];
        self.__nbFailurePrec = [];
        
    def collectData(self, typeF, minPhases, maxPhases, step, avgOver, precision):
        """
        parameters: - typeF: the type of task set desired (ses __typesF for all possibilities)
                    - minPhases: minimal number of phases in a task set
                    - maxPhases: maximal number of phases in a task set
                    - step: size of the increment between each data point collecion
                    - avgOver: number of task set generated for each number of phases (again to get representative results)
                    - precision: number of times the algorithm is run for each task set (to get averaged results)
        """
        self.__minPhases = minPhases;
        self.__maxPhases = maxPhases;
        self.__step = step;
        self.__typeF = typeF;
        for nbPhases in range(minPhases, maxPhases+1, step):
            self.__configuration.minNumberPhases = nbPhases;
            self.__configuration.maxNumberPhases = nbPhases;
            time = 0;
            nbClusters = 0;
            nbSuccess = 0;
            nbErrorsPrec = 0;
            for i in range(avgOver):
                if(typeF != "harmonic"):
                    taskSet = self.__typesF[typeF](self.__configuration);
                else:
                    taskSet = self.__typesF[typeF](self.__configuration,nbPhases);
                taskSet.inisializeRandom();
                exp = Experiment(taskSet);
                exp.run(precision);
                nbClusters += exp.getNbClusters();
                if(exp.isSuccessfull()):
                    nbSuccess += 1;
                if(exp.getPrecisionErrorRation() > 0):
                    nbErrorsPrec += 1;
                time += exp.getTime();
            self.__times.append(time/avgOver);
            self.__nbClusters.append(nbClusters/avgOver);
            self.__nbSuccess.append(nbSuccess/avgOver);
            self.__nbFailurePrec.append(nbErrorsPrec/avgOver);
    
    def save(self, fileName1, fileName2, fileName3, fileName4):
        """
        parameters: -fileName1: name of the file storing the times
                    -fileName2: name of the file storing the number of phase sets
                    -fileName3: name of the file storing the success rates
        """
        with open(fileName1, 'wb') as file0:
            pickle.dump(self.__times, file0);
        with open(fileName2, 'wb') as file0:
            pickle.dump(self.__nbClusters, file0);
        with open(fileName3, 'wb') as file0:
            pickle.dump(self.__nbSuccess, file0);
        with open(fileName4, 'wb') as file0:
            pickle.dump(self.__nbFailurePrec, file0);
    
    def plotTime(self,title):
        fig, ax = plt.subplots();
        ax.plot([i for i in range(self.__minPhases, self.__maxPhases+1, self.__step)],self.__times);
        if(self.__typeF != "harmonic"):
            ax.set(xlabel="number of phases for each task", ylabel="average time taken (nanoseconds)");
        else:
            ax.set(xlabel="base frequency of the tasks", ylabel="average time taken (nanoseconds)");
        ax.grid();
        plt.title(title);
        plt.show();
    
    def plotNbClusters(self,title):
        fig, ax = plt.subplots();
        ax.plot([i for i in range(self.__minPhases, self.__maxPhases+1, self.__step)],self.__nbClusters);
        if(self.__typeF != "harmonic"):
            ax.set(xlabel="number of phases for each task", ylabel="number of partitions");
        else:
            ax.set(xlabel="base frequency of the tasks", ylabel="number of partitions");
        ax.grid();
        plt.title(title);
        plt.show();
    
    def plotSuccess(self,title):
        fig, ax = plt.subplots();
        ax.plot([i for i in range(self.__minPhases, self.__maxPhases+1, self.__step)],self.__nbSuccess);
        if(self.__typeF != "harmonic"):
            ax.set(xlabel="number of phases for each task", ylabel="successe rate");
        else:
            ax.set(xlabel="base frequency of the tasks", ylabel="successe rate");
        ax.grid();
        plt.title(title);
        plt.show();
        
    def plotErrorPrec(self,title):
        fig, ax = plt.subplots();
        ax.plot([i for i in range(self.__minPhases, self.__maxPhases+1, self.__step)],self.__nbFailurePrec);
        if(self.__typeF != "harmonic"):
            ax.set(xlabel="number of phases for each task", ylabel="error rate");
        else:
            ax.set(xlabel="base frequency of the tasks", ylabel="error rate");
        ax.grid();
        plt.title(title);
        plt.show();

if __name__ == "__main__":
    # python3 Experiment general|harmonic|strict nbTasks minPhases maxPhases step avgOver precision name
    # examples (using the time program to get an idea of the total execution time): 
    #     time python3 Experiment.py harmonic 5 10 100 20 1000 2 harmonic
    #     time python3 Experiment.py general 5 10 100 20 1000 2 general
    #     time python3 Experiment.py strict 5 10 100 20 1000 2 strict
    if(len(argv) == 1):
        configuration = ts.Configuration();
        configuration.lowerNbTasksBound = 5;
        configuration.upperNbTasksBound = 5;
        engine = plotter(configuration);
        # Any test code for manual testing
        #engine.collectData("general",10,100,10,1000,100);
        #engine.collectData("general",10,100,10,10,5);
        #engine.collectData("general",10,100,10,1,1); # 10 100 20 1000 2
        #engine.collectData("harmonic",10,100,20,1,1);
        #engine.collectData("strict",10,100,10,1,1);
        #engine.save('times.save', 'clusters.save', 'success.save');
        #engine.plotTime();
        #engine.plotNbClusters();
        #engine.plotSuccess();
    else:
        configuration = ts.Configuration();
        configuration.lowerNbTasksBound = int(argv[2]);
        configuration.upperNbTasksBound = int(argv[2]);
        configuration.maxProcessorSpeed = 7;
        configuration.maximalCPhase = 13;
        engine = plotter(configuration);
        engine.collectData(argv[1],int(argv[3]),int(argv[4]),int(argv[5]),int(argv[6]),int(argv[7]));
        engine.save(argv[8]+'Times.save', argv[8]+'Clusters.save', argv[8]+'Success.save',argv[8]+'FailurePrec.save');
        engine.plotTime(argv[8]);
        engine.plotNbClusters(argv[8]);
        engine.plotSuccess(argv[8]);
        engine.plotErrorPrec(argv[8]);
    
