import Assignment;
import copy;

class SchedulabilityTest:
    """
    Implements the Schedulability test (mainly the creation of the group of phases, the calls to the solver is delegated ot the Assignment class).
    Usage: after construction, call the method run and then use the getters to get the informations.
    """
    
    def __init__(self, taskSet):
        self.partitions = self.buildPartitions(taskSet); # list of list of list, each sub-list is a partition and each two subsub-list contains numTask and associated numPhase at same index
        self.assignments = [];
        for partition in self.partitions:
            self.assignments.append(Assignment.Assignment(partition, taskSet));
        
    def buildPartitions(self, taskSet):
        partitions = [];
        # construction of the boundaries
        boundaries = [];
        for i in range(1,taskSet.getNumberTasks()+1):
            for j in range(1,taskSet.getTask(i).getNumberPhases()+1):
                self.insertSorted(boundaries,taskSet.getTask(i).getOffset() + taskSet.getTask(i).getPhase(j).getOffset());
                self.insertSorted(boundaries,taskSet.getTask(i).getOffset() + taskSet.getTask(i).getPhase(j).getDeadline());
        # construct groups of phases
        for bound in range(0,len(boundaries)-1):
            listOfTask = [];
            listOfPhases = [];
            for i in range(1,taskSet.getNumberTasks()+1):
                for j in range(1,taskSet.getTask(i).getNumberPhases()+1):
                    if((boundaries[bound] <= taskSet.getTask(i).getOffset() + taskSet.getTask(i).getPhase(j).getOffset() and taskSet.getTask(i).getOffset() + taskSet.getTask(i).getPhase(j).getOffset() < boundaries[bound+1]) or (taskSet.getTask(i).getOffset() + taskSet.getTask(i).getPhase(j).getDeadline() <= boundaries[bound+1] and taskSet.getTask(i).getOffset() + taskSet.getTask(i).getPhase(j).getDeadline() > boundaries[bound]) or (taskSet.getTask(i).getOffset() + taskSet.getTask(i).getPhase(j).getDeadline() >= boundaries[bound+1] and taskSet.getTask(i).getOffset() + taskSet.getTask(i).getPhase(j).getOffset() <= boundaries[bound])):
                       listOfTask.append(i);
                       listOfPhases.append(j);
            partition = [];
            partition.append(listOfTask);
            partition.append(listOfPhases);
            partitions.append(partition);
        return partitions;
    
    def insertSorted(self, listOB, bound):
        if(bound not in listOB):
            listOB.append(bound);
            listOB.sort();
        
    def run(self):
        for assignment in self.assignments:
            assignment.solve();
    
    def isSuccessfull(self):
        for assignment in self.assignments:
            if(not assignment.isFeasible()):
                return False;
        return True;
    
    def precisionFailureRatio(self):
        count = 0;
        number = 0;
        for assignment in self.assignments:
            number += 1;
            if((not assignment.getMakeSpan() > 1) and (not assignment.isFeasible())):
                count += 1;
        return count/number;
    
    def getNbClusters(self):
        return len(self.partitions);
    
    def getAssignments(self):
        return copy.deepcopy(self.assignments);
    
    def getPartitions(self):
        return copy.deepcopy(self.partitions);
